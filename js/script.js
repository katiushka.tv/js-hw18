"use strict"
const user = {
    profession:"HR",
    manager: {
        name: "Uasya",
        department: {
            id: 444,
            employees: [
                {
                    name: "Darya",
                    title: "developer",
                },
                {
                    name: "Olya",
                    title: "designer",
                }
            ]
        }
    }
}
function clone (obj) {
    const objClone = {};
    for (let key in obj) {
        objClone[key] = obj[key];
        if (typeof obj[key] === "object" && obj[key] !== null) {
            clone(obj[key]);
        }
    }
    return objClone;
}
const userClone = clone(user);
console.log(user);
console.log(userClone);